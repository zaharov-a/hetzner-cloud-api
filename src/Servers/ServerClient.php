<?php


namespace Ox3a\HetznerCloudApi\Servers;


use Ox3a\HetznerCloudApi\HttpClient;

/**
 * Class ServerClient
 * @package Ox3a\HetznerCloudApi\Servers
 * @property int    $id
 * @property string $name
 * @property string $status
 */
class ServerClient
{
    /**
     * @var HttpClient
     */
    protected $httpClient;

    protected $properties = [
        'id'     => null,
        'name'   => null,
        'status' => null,
    ];


    /**
     * ServerClient constructor.
     * @param HttpClient $httpClient
     */
    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }


    public function __get($name)
    {
        if (array_key_exists($name, $this->properties)) {
            return $this->properties[$name];
        }

        return null;
    }


    private function _set($name, $value)
    {
        if (array_key_exists($name, $this->properties)) {
            $this->properties[$name] = $value;
        }
    }


    public function populate($data)
    {
        foreach (array_keys($this->properties) as $key) {
            $this->properties[$key] = $data->$key;
        }
    }


    public function powerOn()
    {
        $response = $this->httpClient->post("/servers/{$this->id}/actions/poweron");
        $action   = $response->action;
        $this->_set('status', $action->status);

        return $this;
    }


    public function shutdown()
    {
        $response = $this->httpClient->post("/servers/{$this->id}/actions/shutdown");
        $action   = $response->action;
        $this->_set('status', $action->status);

        return $this;
    }


}
