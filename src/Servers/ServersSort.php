<?php


namespace Ox3a\HetznerCloudApi\Servers;


class ServersSort
{
    /**
     * @var string
     */
    protected $field;

    /**
     * @var string
     */
    protected $order;


    public function getValue()
    {
        $value = $this->field;
        if ($this->order) {
            $value .= ":{$this->order}";
        }

        return $value;
    }


    public function byId()
    {
        $this->field = 'id';
        return $this;
    }


    public function byName()
    {
        $this->field = 'name';
        return $this;
    }


    public function byCreated()
    {
        $this->field = 'created';
        return $this;
    }


    public function asc()
    {
        $this->order = 'asc';
        return $this;
    }


    public function desc()
    {
        $this->order = 'desc';
        return $this;
    }


}
