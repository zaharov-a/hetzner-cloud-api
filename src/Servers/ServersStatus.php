<?php


namespace Ox3a\HetznerCloudApi\Servers;


class ServersStatus
{
    const STATUS_INITIALIZING = 'initializing';
    const STATUS_STARTING     = 'starting';
    const STATUS_RUNNING      = 'running';
    const STATUS_STOPPING     = 'stopping';
    const STATUS_OFF          = 'off';
    const STATUS_DELETING     = 'deleting';
    const STATUS_REBUILDING   = 'rebuilding';
    const STATUS_MIGRATING    = 'migrating';
    const STATUS_UNKNOWN      = 'unknown';
    /**
     * @var string
     */
    protected $value;


    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }


    public function initializing()
    {
        $this->value = self::STATUS_INITIALIZING;
        return $this;
    }


    public function starting()
    {
        $this->value = self::STATUS_STARTING;
        return $this;
    }


    public function running()
    {
        $this->value = self::STATUS_RUNNING;
        return $this;
    }


    public function stopping()
    {
        $this->value = self::STATUS_STOPPING;
        return $this;
    }


    public function off()
    {
        $this->value = self::STATUS_OFF;
        return $this;
    }


    public function deleting()
    {
        $this->value = self::STATUS_DELETING;
        return $this;
    }


    public function rebuilding()
    {
        $this->value = self::STATUS_REBUILDING;
        return $this;
    }


    public function migrating()
    {
        $this->value = self::STATUS_MIGRATING;
        return $this;
    }


    public function unknown()
    {
        $this->value = self::STATUS_UNKNOWN;
        return $this;
    }

}
