<?php


namespace Ox3a\HetznerCloudApi\Servers;


use Ox3a\HetznerCloudApi\HttpClient;

class ServersClient
{
    /**
     * @var HttpClient
     */
    protected $httpClient;


    /**
     * ServersClient constructor.
     * @param HttpClient $httpClient
     */
    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }


    /**
     * @param ServersStatus[] $statusList
     * @param ServersSort[]   $sortList
     * @return ServerClient[]
     */
    public function getList($statusList = null, $sortList = null)
    {
        $params = [];
        if ($statusList) {
            $values = [];
            foreach ($statusList as $status) {
                $values[] = $status->getValue();
            }
            $params[] = 'status=' . implode(',', $values);
        }

        if ($sortList) {
            $values = [];
            foreach ($sortList as $sort) {
                $values[] = $sort->getValue();
            }
            $params[] = 'sort=' . implode(',', $values);
        }

        $response = $this->httpClient->get("/servers", implode('&', $params));

        return array_map(
            function ($data) {
                $server = new ServerClient($this->httpClient);

                $server->populate($data);

                return $server;
            },
            $response->servers
        );
    }


    public function get($id)
    {
        $response = $this->httpClient->get("/servers/{$id}");
        $server   = new ServerClient($this->httpClient);

        $server->populate($response->server);

        return $server;
    }


}
