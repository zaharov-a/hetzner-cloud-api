<?php


namespace Ox3a\HetznerCloudApi;


use Ox3a\HetznerCloudApi\Servers\ServersClient;

class ApiClient
{
    /**
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * @var ServersClient
     */
    protected $servers;


    public function __construct($token)
    {
        $this->httpClient = new HttpClient($token);
    }


    public function getServers()
    {
        if (!$this->servers) {
            $this->servers = new ServersClient($this->httpClient);
        }

        return $this->servers;
    }
}
