<?php


namespace Ox3a\HetznerCloudApi;


use http\QueryString;
use RuntimeException;
use Zend\Http\Header\Authorization;
use Zend\Http\Request;
use Zend\Stdlib\Parameters;

class HttpClient
{
    /**
     * @var string
     */
    protected $token;

    protected $url = 'https://api.hetzner.cloud/v1';
    // protected $url = 'https://gate.0x3a.xyz/headers.php';


    /**
     * HttpClient constructor.
     * @param string $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }


    public function query(Request $request)
    {
        // printf("%s\n\n", $request->toString());
        $ch = curl_init($request->getUriString());

        if ($request->isPost()) {
            curl_setopt($ch, CURLOPT_POST, true);
        }

        $headers = [];
        foreach ($request->getHeaders() as $header) {
            $headers[] = $header->toString();
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);

        if (($error = curl_error($ch))) {
            $code = curl_errno($ch);
            curl_close($ch);
            throw new RuntimeException($error, $code);
        }
        curl_close($ch);

        $response = json_decode($result);
        if (isset($response->error)) {
            throw new RuntimeException($response->error->message);
        }

        return $response;
    }


    public function createRequest($query, $params = [])
    {
        $request = new Request();
        $request->getHeaders()->addHeader(new Authorization("Bearer {$this->token}"));
        $request->setUri("{$this->url}$query");

        $request->getUri()->setQuery($params);

        return $request;
    }


    public function get($query, $params = [])
    {
        $request = $this->createRequest($query, $params);
        $request->setMethod('GET');

        return $this->query($request);
    }


    public function post($query, $params = [])
    {
        $request = $this->createRequest($query, $params);
        $request->setMethod('POST');

        return $this->query($request);
    }

}
