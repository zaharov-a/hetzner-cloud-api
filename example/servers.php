<?php

use Ox3a\HetznerCloudApi\ApiClient;
use Ox3a\HetznerCloudApi\Servers\ServersSort;
use Ox3a\HetznerCloudApi\Servers\ServersStatus;

require __DIR__ . '/../vendor/autoload.php';

$config = require __DIR__ . '/config.cfg.php';

$client = new ApiClient($config['token']);

$servers = $client->getServers();
// $server = $servers->get($id);
$list = $servers->getList(
    [
        (new ServersStatus())->deleting(),
        (new ServersStatus())->rebuilding(),
        (new ServersStatus())->running(),
    ],
    [
        (new ServersSort())->byId()->desc(),
        (new ServersSort())->byName()->asc(),
    ]
);
$server = $servers->getList()[0];
sleep(5);
$server->shutdown();
sleep(15);
$server->powerOn();
print_r($server);
